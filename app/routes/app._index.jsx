import React from 'react';
import Index from './Index'; // Assuming Index.js is in the same directory

const App = () => {
  return (
    <div>
      <h1>My Shopify App</h1>
      <Index />
    </div>
  );
};

export default App;
