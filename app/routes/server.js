import express from 'express';
import axios from 'axios';
import cors from 'cors';

const app = express();
const PORT = 3000;

const SHOP = 'magyk-test.myshopify.com';
const ACCESS_TOKEN = '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';

app.use(cors());

app.get('/product', async (req, res) => {
    try {
        const response = await axios.get(`https://${SHOP}/admin/api/2023-01/products.json`, {
            headers: {
                'X-Shopify-Access-Token': ACCESS_TOKEN,
            },
        });
        const product = response.data.products[0];
        const productInfo = {
            name: product.title,
            description: product.body_html,
        };
        res.json(productInfo);
    } catch (error) {
        console.error('Error fetching product:', error.message);
        res.status(500).send('Error fetching product.');
    }
});

app.get('/products', async (req, res) => {
    try {
        const response = await axios.get(`https://${SHOP}/admin/api/2023-01/products.json`, {
            headers: {
                'X-Shopify-Access-Token': ACCESS_TOKEN,
            },
        });

        if (response.data && response.data.products) {
            const products = response.data.products.map(product => ({
                name: product.title,
                description: product.body_html,
            }));
            res.json(products);
        } else {
            res.status(404).send('No products found.');
        }
    } catch (error) {
        console.error('Error fetching products:', error.message);
        res.status(500).send('Error fetching products.');
    }
});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
