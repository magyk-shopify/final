import React, { useState } from 'react';
import axios from 'axios';
import { Page, Button, Text, Box } from '@shopify/polaris';

const Index = () => {
  const [product, setProduct] = useState(null);
  const [products, setProducts] = useState([]);

  const fetchProduct = async () => {
    try {
      const response = await axios.get('http://localhost:3000/product');
      setProduct(response.data);
    } catch (error) {
      console.error('Error fetching product:', error);
    }
  };

  const fetchAllProducts = async () => {
    try {
      const response = await axios.get('http://localhost:3000/products');
      setProducts(response.data);
    } catch (error) {
      console.error('Error fetching products:', error);
    }
  };

  return (
    <Page>
      <Button onClick={fetchProduct}>Fetch Product</Button>
      <Button onClick={fetchAllProducts}>Fetch All Products</Button>

      {product && (
        <Box>
          <Text>{product.name}</Text>
          <Text>{product.description}</Text>
        </Box>
      )}

      {products.length > 0 && (
        <Box>
          {products.map((product, index) => (
            <Box key={index}>
              <Text>{product.name}</Text>
              <Text>{product.description}</Text>
            </Box>
          ))}
        </Box>
      )}
    </Page>
  );
};

export default Index;
